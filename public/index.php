<?php
//error_reporting(E_ERROR);
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Autoload del framework
require '../vendor/autoload.php';

//Incluye archivos de configuracion
require '../src/config/db.php';

//Incluye librerias
require '../src/lib/tokens.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;


$app = new \Slim\App(['settings' => $config]);


// Personalizacion del error 405 Method not allowed
$c = $app->getContainer();
$c['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        $data['Error'] = "Method not allowed";
        echo json_encode($data);

        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withHeader('Content-type', 'application/json');
    };
};

//Middleware de autenticación con token
$app->add(function($request, $response, $next){

    $clientAuthorization = $request->getHeaderLine('auth-token');
    
    $tk = new token();
    $tk->setToken();
    $tokenAPI = $tk->getToken();

    $data['Error'] = 'OK';

    if ($request->getContentType() != 'application/json'){
        $data['Error'] = "El content-type debe ser application/json";
        $response = $response->withStatus(415);
        echo json_encode($data);
    }else{
        
        if ($tokenAPI == $clientAuthorization){
            $response = $next($request, $response);
        }else{
            $response = $response->withStatus(401);
            $response = $response->withHeader('Content-type', 'application/json');
            $data['Error'] = "No se encuentra el token de seguridad";
            echo json_encode($data);
        }
    }
    
    return $response;
});

//Crear las rutas para los clientes
$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name, desde login");
    return $response;
});

require '../src/routes/test.php';
require '../src/routes/company.php';
require '../src/routes/courses.php';
require '../src/routes/knowledge_areas.php';
require '../src/routes/students.php';
require '../src/routes/self_service.php';

require '../src/lib/validators.php'; //Validaciones de campo


$app->run();