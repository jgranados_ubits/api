<?php


/**
 * Clase de conexion a la base de datos usando PDO
 * @author John W Granados A - jwga76@gmail.com - @master_mintaka
 */


class db{
    private $host = 'localhost';
    private $usuario = 'root';
    private $password = '12345';
    private $database = 'bitnami_moodle';


    //Conectar DB
    public function conectar(){
        $conexion_mysql ="mysql:host=$this->host;dbname=$this->database";
        $conexionDB = new PDO($conexion_mysql, $this->usuario, $this->password);
        $conexionDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //Arregla la codificacion UTF-8
        $conexionDB -> exec("Set names utf8");
        return $conexionDB;
    }
}
/* 
//Test DB Connection
 $test_db = new db();
$conn = $test_db->conectar();

var_dump($conn);

 */