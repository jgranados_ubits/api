<?php

/** Company routes 
 * @author John W Granados
 * @date 2019-12-02   
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../src/models/companyModel.php';

$app->group('/company', function () use ($app) {

    $app->get('/test', function(Request $request, Response $response){
        $response->getBody()->write("company -> test OK");
        return $response;
    });

    // Company info
    $app->get('/by_id', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $companyModel = new companyModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $student = $companyModel->companyById($id_company);
            
            if(!empty($student[0])){
                echo json_encode($student[0]);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode($datos);
            }
            
        }
        return $response;
    });

    // Iniciated courses
    $app->get('/quantity_iniciated_courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $companyModel = new companyModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $companyModel->getQuantityIniciatedCourses($id_company);
            echo json_encode($data[0]);
        }
        return $response;
    });

    //List of iniciated courses
    $app->get('/list_iniciated_courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $companyModel = new companyModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $companyModel->getListIniciatedCourses($id_company);
            
            if(count($data)>0){
                echo json_encode($data);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode($datos);
            }
        }
        return $response;
    });

    // Finished courses
    $app->get('/quantity_finished_courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $companyModel = new companyModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $companyModel->getQuantityFinishedCourses($id_company);
            echo json_encode($data[0]);
        }
        return $response;
    });

    //List of finished courses
    $app->get('/list_finished_courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $companyModel = new companyModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $companyModel->getListFinishedCourses($id_company);
            if(count($data)>0){
                echo json_encode($data);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode($datos);
            }
        }
        return $response;
    });

    //Percentage finished courses
    $app->get('/percentage_finished_courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $companyModel = new companyModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $finished = $companyModel->getQuantityFinishedCourses($id_company);
            $started  = $companyModel->getQuantityIniciatedCourses($id_company);
            
            if ((int)$started[0]->started > 0){
                $percentage = round( ((int)$finished[0]->finished / (int)$started[0]->started) * 100 , 2);
            }else{
                $percentage = 0;
            }
            

            $data = ["percentage" => (string)$percentage ];
            echo json_encode($data);
        }
        return $response;
    });

    //Percentage finished courses
    $app->get('/accomplishment_percentage', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $companyModel = new companyModel();
        
        $data = $request->getParsedBody();  
        $id_company = $data['id_company'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $expected = $companyModel->getAccomplishmentPercentage($id_company);
            $real_time  = $companyModel->getAllStudentsTime($id_company, $start_date, $finish_date);
            
            if ((int)$expected[0]->expected_time > 0){
                $percentage = round( ((int)$real_time[0]->time_mins / (int)$expected[0]->expected_time) * 100 , 2);
            }else{
                $percentage = 0;
            }
            $data['id_company'] = (string)$data["id_company"];
            $data["expected_time_mins"] = $expected[0]->expected_time;
            $data["real_time_mins"] = $real_time[0]->time_mins;
            $data["percentage"] = (string)$percentage;
            echo json_encode($data);
        }
        return $response;
    });

});//fin grupo
