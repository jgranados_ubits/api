<?php
/**
 * Rutas para la tabla Autenticacion
 * @author John W Granados A - jwga76@gmail.com - @master_mintaka
 * 2018-12-06
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../src/models/userModel.php';




$app->group('/login', function () use ($app) {
    

    /**
     * Ruta de prueba, recibe un parametro en la URL
     *  @author John W Granados A - jwga76@gmail.com - @master_mintaka
     * 2018-06-29
     */
    $app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
        $name = $args['name'];
        $response->getBody()->write("Hello, $name, desde login");
        return $response;
    });

    /**
     * Login and authentication
     * @author John W Granados A - jwga76@gmail.com - @master_mintaka
     * 2018-12-06
     */
    $app->get('/login', function (Request $request, Response $response, array $args) {
        $response = $response->withHeader('Content-type', 'application/json');
        $userModel = new userModel();
       
        $data = $request->getParsedBody();
        $login = $data['login'];
        $password= $data['password'];

        $val = new validators();
        
        $val->validateLength('login', $login, 5);
        $val->validateLength('password', $password, 5);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validación de campos";
            $datos['Descripción'] = $val->validations;
            echo json_encode($datos);
        }else{
            $salida = "Login: ".$login." Password: ".$password;
            //Aqui debemos poner el acceso al modelo y validar los datos de autenticación
            //Logica del password
            $auth = $userModel->user_auth($login, $password);
            echo json_encode($auth);
        }
        return $response;
    });
    
    /**
     * Inserta un registro en la tabla ust_agentes, los datos debn ser enviados por POST
     * @author John W Granados A - jwga76@gmail.com- @master_mintaka
     */
    $app->post('/crearAgenda', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $agendaModel = new agendaModel();

        //Validación de campos
        //date_start varchar 2018-01-01 00:00:00 longitud 19
        $data = $request->getParsedBody();

        $val = new validators();

        $val->validateLength('date_make', $data['date_make'], 0, false); //No obligatorio
        $val->validateLength('idcall', $data['idcall'], 36);
        $val->validateLength('status', $data['status'], 5, false);
        $val->validateLength('date_start', $data['date_start'], 19);
        $val->validateLength('date_exec', $data['date_exec'], 0, false); //No obligatorio
        $val->validateLength('assigned_user_id', $data['assigned_user_id'], 36);
        $val->validateLength('parent_id', $data['parent_id'], 36);
        $val->validateLength('Parent_type', $data['Parent_type'], null);
        $val->validateLength('first_name', $data['first_name'], null);
        $val->validateLength('last_name', $data['last_name'], null);
        $val->validateLength('phone', $data['phone'], 10);
        $val->validateLength('prioridad_c', $data['prioridad_c'], null);

        //Validaciones de datos
        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validación de campos";
            $datos['Descripción'] = $val->validations;
            echo json_encode($datos);

        }else{

            try{
                $msg_insert = $agendaModel->insertAgenda($request);

                //Exportar a JSON
                $datos['msg'] = $msg_insert['resultado'];
                $datos['ID'] = $msg_insert['lastId'];
                //$datos['Authorization'] = $request->getHeaderLine('Authorization');
                
            } catch(PDOException $e){
                $datos["Error"] = $e->getMessage();
            }
            echo json_encode($datos);
            
            
        }
        return $response;
        
    });

    /**
     * Describe la tabla call_agenda de la db del pbx, éstos datos se regresan a Gear Plug
     * @author John W Granados A - jwga76@gmail.com
     * 2018-06-28
     */
    $app->get('/describe', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        
        try{
            $agendaModel = new agendaModel();
            $agentes = $agendaModel->describeAgenda();
    
            //Exportar a JSON
            $datos['msg'] = true;
            $datos['tabla'] = $agentes;
            
        } catch(PDOException $e){
            $datos["Error"] = $e->getMessage();
        }
        echo json_encode($datos);
        return $response;
    });

});//fin grupo