<?php

/** self service routes 
 * @author John W Granados
 * @date 2019-12-12   
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../src/models/selfServiceModel.php';

$app->group('/self_service', function () use ($app) {

    $app->get('/test', function(Request $request, Response $response){
        $response->getBody()->write("Self Service -> test OK");
        return $response;
    });


    //Get student's company'
    $app->post('/create_company', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $selfServiceModel = new selfServiceModel();
       
        $data = $request->getParsedBody();

        $name = $data['name'];
        $description = $data['description'];
        $alias = $data['alias'];

        $val = new validators();
        //Campos obligatorios
        //$val->isRequired('name',$name);
        $val->isRequired('description',$description);
        //$val->isRequired('alias',$alias);

        $val->validateLength('name', $name, 1);
        $val->validateLength('alias', $alias, 1);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            try{
                $msg_insert = $selfServiceModel->createCompany($request);

                //Exportar a JSON - Data creation response 
                $datos['msg'] = $msg_insert['resultado'];
                $datos['ID'] = $msg_insert['lastId'];
                $datos['attributes'] = $msg_insert['attributes'];
                $datos['Authorization'] = $request->getHeaderLine('Authorization');
                
            } catch(PDOException $e){
                $datos["Error"] = $e->getMessage();
            }
            echo json_encode($datos);
        }
        return $response;
    });

//Get student's company'
$app->post('/create_user', function(Request $request, Response $response){
    $response = $response->withHeader('Content-type', 'application/json');
    $selfServiceModel = new selfServiceModel();
   
    $data = $request->getParsedBody();

    $username = $data['username'];
    $firstname = $data['firstname'];
    $lastname = $data['lastname'];
    $email = $data['email'];
    $city = $data['city'];
    $country = $data['country'];
    $role = $data['role'];
 
    $val = new validators();
    //Campos obligatorios
    //$val->isRequired('username',$username);
    $val->validateLength('username', $username, 4);
    //$val->isRequired('firstname',$firstname);
    $val->validateLength('firstname', $firstname, 2);
    //$val->isRequired('lastname',$lastname);
    $val->validateLength('lastname', $lastname, 2);
    //$val->isRequired('email',$email);
    $val->validateLength('email', $email, 2); #TODO validació para campo de correo
    //$val->isRequired('city',$city);
    $val->validateLength('city', $city, 4);
    //$val->isRequired('country',$country);
    $val->validateLength('country', $country, 1);
    //$val->isRequired('role',$role);
    $val->validateLength('role', $role, 3);

    
    if ($val->validations){
        $response = $response->withStatus(202);
        $datos['Error'] = "Validation";
        $datos['Description'] = $val->validations;
        echo json_encode($datos);
    }else{
        try{
            $msg_insert = $selfServiceModel->createUser($request);

            //Exportar a JSON - Data creation response 
            $datos['msg'] = $msg_insert['resultado'];
            $datos['lastId'] = $msg_insert['lastId'];
            $datos['preferences'] = $msg_insert['preferences'];
            
        } catch(PDOException $e){
            $datos["Error"] = $e->getMessage();
        }
        echo json_encode($datos);
    }
    return $response;
});

});//fin grupo