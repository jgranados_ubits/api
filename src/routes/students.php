<?php

/** students routes 
 * @author John W Granados
 * @date 2019-12-02   
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../src/models/studentsModel.php';

$app->group('/get_student', function () use ($app) {

    $app->get('/test', function(Request $request, Response $response){
        $response->getBody()->write("students -> test OK");
        return $response;
    });


    //Get student's company'
    $app->get('/company', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];

        $val = new validators();
        //$val->isRequired('id_user',$id_user);
        $val->validateLength('id_user', $id_user, 1);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $studentModel->studentIdCompany($id_user);
            if (!empty($data[0])){
                echo json_encode($data[0]);
            }else{
                echo json_encode(null);
            }
            
        }
        return $response;
    });

    //Get student by username
    $app->get('/by_username', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $username = $data['username'];

        $val = new validators();
        //$val->isRequired('username',$username);
        $val->validateLength('username', $username, 3);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $studentModel->studentByUserName($username);
            if (!empty($data[0])){
                echo json_encode($data[0]);
            }else{
                echo json_encode(null);
            }
        }
        return $response;
    });

    //Get student by id
    $app->get('/by_id', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];

        $val = new validators();
        //$val->isRequired('id',$id_user);
        $val->validateLength('id', $id_user, 1);


        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $studentModel->studentById($id_user);
            if (!empty($data[0])){
                echo json_encode($data[0]);
            }else{
                echo json_encode(null);
            }
        }
        return $response;
    });

    //Get student by email
    $app->get('/by_email', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $email = $data['email'];

        $val = new validators();
        //$val->isRequired('id',$email);
        $val->validateLength('id', $email, 1);


        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $studentModel->studentByEmail($email);
            if (!empty($data[0])){
                echo json_encode($data[0]);
            }else{
                $response = $response->withStatus(206);
                $data["error"] = "El usuario no existe";
                echo json_encode($data);
            }
        }
        return $response;
    });
    
    //get annual goal
    $app->get('/annual_goal', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        //$val->isRequired('id_user',$id_user);
        $val->validateLength('id_user', $id_user, 0);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $annual_goal = $studentModel->studentAnnualGoal($id_user);
            $datos['annual_goal'] = (int)$annual_goal->min_time_on_platform;
            
        }

        $studied_time = $studentModel->getCoursesTime($id_user, '', '', $start_date, $finish_date);
        $datos['studied_time'] = (float)$studied_time->time;
        $datos['studied_percenteje'] = round(($studied_time->time / $annual_goal->min_time_on_platform ) * 100, 2);

        echo json_encode($datos);


        return $response;
    });

    //get monthly goal
    $app->get('/monthly_goal', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        //$val->isRequired('id_user',$id_user);
        $val->validateLength('id_user', $id_user, 0);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $studentModel->studentMonthlyGoal($id_user);
            $datos['monthly_goal'] = (int)$data->monthly_min_time_on_platform;
        }

        $studied_time = $studentModel->getCoursesTime($id_user, '', '', $start_date, $finish_date);
        $datos['studied_time'] = (float)$studied_time->time;
        $datos['studied_percenteje'] = round(($studied_time->time / $data->monthly_min_time_on_platform ) * 100, 2);

        echo json_encode($datos);
        return $response;
    });

    //get courses time
    $app->get('/courses_time', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];
        $compulsory = $data['compulsory'];
        $private = $data['private'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        //$val->isRequired('id_user',$id_user);
        $val->validateLength('id_user', $id_user, 0);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $courses_time = $studentModel->getCoursesTime($id_user, $compulsory, $private, $start_date, $finish_date);
            $datos['time_mins'] = $courses_time;
            // $datos['course_type'] = $course_type;
            echo json_encode($datos);
        }
        return $response;
    });  

    //get list courses time
    $app->get('/list_courses_time', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];
        $compulsory = $data['compulsory'];
        $private = $data['private'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        //$val->isRequired('id_user',$id_user);
        $val->validateLength('id_user', $id_user, 0);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $courses_time = $studentModel->getListCoursesTime($id_user, $compulsory, $private, $start_date, $finish_date);
            if ( isset($courses_time) && count($courses_time) > 0 ){
                echo json_encode($courses_time);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode($datos);
            }
            
        }
        return $response;
    });  

    
    //get courses time
    $app->get('/compulsory_courses_time', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        //$val->isRequired('id_user',$id_user);
        $val->validateLength('id_user', $id_user, 0);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $courses_time = $studentModel->getCompulsoryCoursesTime($id_user, $start_date, $finish_date);
            $datos['time_mins'] = $courses_time->time_mins;
            echo json_encode($datos);
        }
        return $response;
    });  

    //get list courses time
    $app->get('/list_compulsory_courses_time', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
        $id_user = '';
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        //$val->isRequired('id_user',$id_user);
        $val->validateLength('id_user', $id_user, 0);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $courses_time = $studentModel->getListCompulsoryCoursesTime($id_user, $start_date, $finish_date);
            if (count($courses_time) > 0){
                echo json_encode($courses_time);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode($datos);
            }
        }
        return $response;
    });

    //Get student by email
    $app->get('/courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
       
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];

        $val = new validators();
        $val->validateLength('id', $id_user, 0);

        if ($val->validations){
            $response = $response->withStatus(401);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $data = $studentModel->coursesByStudent($id_user);
            if (!empty($data[0])){
                echo json_encode($data);
            }else{
                $response = $response->withStatus(206);
                $data["error"] = "El usuario no existe";
                echo json_encode($data);
            }
        }
        return $response;
    });

    //get list courses time
    $app->get('/list_approved_courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $studentModel = new studentsModel();
        $id_user = '';
        $data = $request->getParsedBody();
        $id_user = $data['id_user'];
        $start_date = $data['start_date'];
        $finish_date = $data['finish_date'];

        $val = new validators();
        $val->validateLength('id_user', $id_user, 0);
        $val->validateLength('start_date', $start_date, 9);
        $val->validateLength('finish_date', $finish_date, 9);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $courses_time = $studentModel->getListApprovedCourses($id_user, $start_date, $finish_date);
            if (count($courses_time) > 0){
                echo json_encode($courses_time);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode($datos);
            }
        }
        return $response;
    });

});//fin grupo