<?php

/** Knowledge Areas routes 
 * @author John W Granados
 * @date 2019-12-02   
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../src/models/knowledgeAreasModel.php';

$app->group('/knowledge_areas', function () use ($app) {

    $app->get('/test', function(Request $request, Response $response){
        $response->getBody()->write("knowledge_areas -> test OK");
        return $response;
    });

    // List of knowledge areas
    $app->get('/list', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $knowledgeAreasModel = new knowledgeAreasModel();

        $student = $knowledgeAreasModel->knowledgeAreaslist();
        echo json_encode($student);
        return $response;
    });

    $app->get('/courses', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $knowledgeAreasModel = new knowledgeAreasModel();
       
        $data = $request->getParsedBody();
        $id_knowledge_area = $data['id_knowledge_area'];

        $val = new validators();
        $val->isRequired('id_knowledge_area',$id_knowledge_area);
        //$val->validateLength('id_knowledge_area', $id_knowledge_area, 0);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $courses = $knowledgeAreasModel->coursesByknowledgeArea($id_knowledge_area);
            
            if(count($courses) > 0){
                echo json_encode($courses);
            }else{
                $datos["error"] = 'No hay datos para mostrar';
                echo json_encode($datos);
            }
            
        }
        return $response;
    });
    
    

});//fin grupo
