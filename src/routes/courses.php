<?php

/** Courses routes 
 * @author John W Granados
 * @date 2019-12-02   
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../src/models/courseModel.php';

$app->group('/get_courses', function () use ($app) {

    $app->get('/test', function(Request $request, Response $response){
        $response->getBody()->write("course -> test OK");
        echo json_encode($_SERVER);
        return $response;
    });

    //Get course info
    $app->get('/info', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $courseModel = new courseModel();
       
        $data = $request->getParsedBody();
        $id_course = $data['id_course'];

        $val = new validators();
        //$val->isRequired('id_course',$id_course);
        $val->validateLength('id_course', $id_course, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $info = $courseModel->getCourse($id_course);
            
            if(!empty($info[0])){
                echo json_encode($info[0]);
            }else{
                $datos["error"] = "No se encontró el curso";
                echo json_encode($datos);
            }
                
        }
        return $response;
    });

////////////////////////////////////////////////////////////////////
// Datos de cursos Ubits
////////////////////////////////////////////////////////////////////

    //Get courses list
    #TODO verificar la consulta que este trayendo unicamente los cursos de ubits, sin importar si son obligatorios o no
    $app->get('/list', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $courseModel = new courseModel();

        $student = $courseModel->listUbitsCourses();
        echo json_encode($student);
        return $response;   
    });

    $app->get('/quantity', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $courseModel = new courseModel();

        $student = $courseModel->quantityUbitsCourses();
        echo json_encode($student[0]);
        return $response;   
    });

////////////////////////////////////////////////////////////////////////////
//Datos por empresa
////////////////////////////////////////////////////////////////////////////

    //Get own courses list by company
    $app->get('/by_company', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $courseModel = new courseModel();
    
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $list_own_courses = $courseModel->getListOwnCourses($id_company);
            
            if(!empty($list_own_courses) > 0){
                echo json_encode($list_own_courses);
            }else{
                $datos['error'] = "No hay información para mostrar";
                echo json_encode($datos);
            }
                
        }
        return $response;  
    });

    //Get own courses quantity by company
    $app->get('/quantity_by_company', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $courseModel = new courseModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $quantity_own_courses = $courseModel->getQuantityOwnCourses($id_company);
            echo json_encode($quantity_own_courses[0]);
        }
        return $response;
    });



////////////////////////////////////////////////////////////////////
//Cursos obligatorios por empresa
////////////////////////////////////////////////////////////////////

    //Get quantity compulsory courses students
    $app->get('/quantity_compulsory_courses_students', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $courseModel = new courseModel();
       
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $student = $courseModel->getQuantityCompulsoryCoursesStudents($id_company);
            
            if(count($student) > 0){
                echo json_encode($student);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode ($datos);
            }
            
        }
        return $response;
    });

    //Get list of compulsory courses from company students
    $app->get('/list_compulsory_courses_students', function(Request $request, Response $response){
        $response = $response->withHeader('Content-type', 'application/json');
        $courseModel = new courseModel();
        
        $data = $request->getParsedBody();
        $id_company = $data['id_company'];

        $val = new validators();
        //$val->isRequired('id_company',$id_company);
        $val->validateLength('id_company', $id_company, 1);

        if ($val->validations){
            $response = $response->withStatus(202);
            $datos['Error'] = "Validation";
            $datos['Description'] = $val->validations;
            echo json_encode($datos);
        }else{
            $student = $courseModel->getListCompulsoryCoursesStudents($id_company);
            
            if(count($student) > 0){
                echo json_encode($student);
            }else{
                $datos["error"] = "No hay datos para mostrar";
                echo json_encode ($datos);
            }
        }
        return $response;
    });
    

});//fin grupo

