<?php
/**
 * Rutas para la tabla Autenticacion
 * @author John W Granados A - jwga76@gmail.com - @master_mintaka
 * 2018-12-06
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../src/models/test_model.php';


/**
 * Ruta de prueba, recibe un parametro en la URL
 *  @author John W Granados A - jwga76@gmail.com - @master_mintaka
 * 2018-06-29
 */
$app->get('/hello2/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name, desde login");
    return $response;
});

/**
 * Test sql query
 * @author John W Granados A - jwga76@gmail.com
 * 2019-11-29
 */
$app->get('/test', function(Request $request, Response $response){
    $response = $response->withHeader('Content-type', 'application/json');
    
    try{
        $testModel = new testModel();
        $test = $testModel->testQuery();

        //Exportar a JSON
        $datos['msg'] = true;
        $datos['tabla'] = $test;
        
    } catch(PDOException $e){
        $datos["Error"] = $e->getMessage();
    }
    echo json_encode($datos);
    return $response;
});
