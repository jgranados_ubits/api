<?php

class validators{

    //Atributos
    public $validations = false;

    //Metodos

    /**
     * Validación para campos vacíos
     */
    function isRequired($field_name, $value){
        if(strlen($value)==0 ){
            $this->validations[$field_name][] = 'Campo obligatorio'; 
        }
    }

    /**
     * Valida variables con formato datetime, debe tener 19 caracteres exactos
     * @author John W Granados A - jwga76@gmail.com - @master_mintaka
     * @param $value string con la fecha
     */
    public function validateDateTime($field_name, $value, $required){

        if ($required) $this->isRequired($field_name, $value);

        if (strlen($value) != 19 ) {
            $this->validations[$field_name][] = 'Formato datetime yyyy-mm-dd hh:mm:ss' ;
        }

    }//fin function validateDateTime


    public function validateLength($field_name, $value, $length, $required = true){
        if ($required) {
            $this->isRequired($field_name, $value);
        }
        if(isset($length) && strlen($value) <= $length){
            $this->validations[$field_name][] = 'Longitud esperada: '.$length ;
        }
    }//fin function validateLength

   

}//fin class validators


?>