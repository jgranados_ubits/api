<?php

class token{

    private $token;

    public function setToken( $token = '' ){
        if (empty($token)){
            $this->token = "YekB8wrXxhrkANQqYkElikipsy3gsVN1gq02y2ibNFgRGKt9rxz8q6ZYbeupXjaf3wGMAY73bnLjZSNCdkF1A==";
        }else{
            $this->token = $token;
        }
        
    }

    public function getToken(){
        return $this->token;
    }

    public function generateToken(){
        $noValidChars = array('/', '+');
        $tokenNew = str_replace($noValidChars, '', base64_encode(random_bytes(64)));
        $this->setToken($tokenNew);
    }

    public function tokenVerify($tokenClient, $tokenAPI){
        if ($tokenClient === $tokenAPI){
            return true;
        }else{
            return false;
        }
    }//fin function tokenVerify


}//fin class tokens

?>