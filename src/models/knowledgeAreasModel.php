<?php


/**
 * 
 */
class knowledgeAreasModel extends db{
    private $con_obj;
    private $connection;

    public function __construct(){
        
    }

    public function knowledgeAreaslist(){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT * 
        FROM mdl_course_categories 
        WHERE depth in (1,2)
        AND parent != 2";

        try{
            $execute = $this->connection->query($sqlstr);
            $kwonledgeAreas = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $kwonledgeAreas;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function coursesByknowledgeArea($id_knowledge_area){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        #TODO Poner la consulta adecuada
        $sqlstr = "SELECT * 
        FROM mdl_course
        WHERE category = $id_knowledge_area";

        try{
            $execute = $this->connection->query($sqlstr);
            $kwonledgeAreas = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $kwonledgeAreas;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

   

}//fin class studentModel

