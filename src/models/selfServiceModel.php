<?php


/**
 * 
 */
class selfServiceModel extends db{
    private $con_obj;
    private $connection;

    public function __construct(){
        
    }

    public function createCompany($request){
        $data = $request->getParsedBody();

        $name = filter_var($data['name'], FILTER_SANITIZE_STRING);
        $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
        $nit = (!empty($data['nit'])) ? filter_var($data['nit'], FILTER_SANITIZE_STRING): '';
        $alias = filter_var($data['alias'], FILTER_SANITIZE_STRING);
        $min_time_on_platform = (!empty($data['min_time_on_platform'])) ? $data['min_time_on_platform'] : 720;
        $repeat_hanco = (!empty($data['repeat_hanco'])) ? $data['repeat_hanco'] : 1;
        $own_courses_number = 0; //No se expone
        $max_contents_size = 10485760; //No se expone

        try{
            $this->con_obj = new db();
            $this->connection = $this->con_obj->conectar();

            //Insert MDL_U_COMPANY
            $sqlstr = "INSERT INTO mdl_u_company (
                `name`,
                `description`,
                `nit`,
                `min_time_on_platform`,
                `alias`,
                `repeat_hanco`,
                `own_courses_number`,
                `max_contents_size`
                ) 
            VALUES (
                :name, 
                :description, 
                :nit, 
                :min_time_on_platform,
                :alias,
                :repeat_hanco,
                :own_courses_number,
                :max_contents_size
                )";
            
            $stmt = $this->connection->prepare($sqlstr);
            $stmt->bindParam("name", $name);
            $stmt->bindParam("description", $description);
            $stmt->bindParam("nit", $nit);
            $stmt->bindParam("min_time_on_platform", $min_time_on_platform);
            $stmt->bindParam("alias", $alias);
            $stmt->bindParam("repeat_hanco", $repeat_hanco);
            $stmt->bindParam("own_courses_number", $own_courses_number);
            $stmt->bindParam("max_contents_size", $max_contents_size);
            $stmt->execute();
 
            $last_id = $this->connection->lastInsertId();

            //MDL_U_USER_ADDITIONALINFO
            $attributes = $this->createCompanyAttributes($request, $last_id);

            //MDL_COURSE_CATEGORIES
            $nextid = $this->createCourseCategory($request);
            
            $this->connection = NULL; //Cierra la conexion
            
            $data['resultado'] = true;
            $data['lastId'] = $last_id;
            $data['attributes'] = $attributes['resultado'];
            $data['nextid'] = $nextid;
        } catch(PDOException $e){
            $data['resultado'] = $e->getMessage() ;
            $data['lastId'] = "error";
        }
        return $data;

    }//fin public function insertAgenda

    public function createCompanyAttributes($request, $id_company){
        $data_request = $request->getParsedBody();

        foreach($data_request as $key => $value) {
            $value = filter_var($data_request[$key], FILTER_SANITIZE_STRING);

            try{
                $this->con_obj = new db();
                $this->connection = $this->con_obj->conectar();

                //Insert MDL_U_COMPANY
                $sqlstr = "INSERT INTO mdl_u_company_attribute (
                    `name`,
                    `value`,
                    `mdl_u_company_id`
                    ) 
                VALUES (
                    :name, 
                    :value, 
                    :mdl_u_company_id
                    )";
                
                $stmt = $this->connection->prepare($sqlstr);
                $stmt->bindParam("name", $key);
                $stmt->bindParam("value", $value);
                $stmt->bindParam("mdl_u_company_id", $id_company);
                $stmt->execute();

                $this->connection = NULL; //Cierra la conexion
                
                $data['resultado'] = true;
            } catch(PDOException $e){
                $data['resultado'] = $e->getMessage() ;
            } 
        }
        
        return $data;

    }//fin public function createCompanyAttributes

    public function getNextAvaiableKnowledgeAreaID(){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT auto_increment FROM information_schema.tables WHERE table_name='mdl_course_categories'";;

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetch(PDO::FETCH_NUM);
            $this->connection = NULL; //Cierra la conexion
            return (int)$resp[0];

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function createCourseCategory($request){
        $data = $request->getParsedBody();

        $nextid = $this->getNextAvaiableKnowledgeAreaID();
        $alias = filter_var($data['alias'], FILTER_SANITIZE_STRING);
        $parent = 2;
        $timemodified = time();
        $depth = 2;
        $path = "$depth/$nextid";
        
        try{
            $this->con_obj = new db();
            $this->connection = $this->con_obj->conectar();
            
            //Insert MDL_U_COMPANY
            $sqlstr = "INSERT INTO mdl_course_categories (
                `id`,
                `name`,
                `parent`,
                `timemodified`,
                `depth`,
                `path`
                ) 
            VALUES (
                :id, 
                :name, 
                :parent,
                :timemodified,
                :depth,
                :path
                )";
            
            $stmt = $this->connection->prepare($sqlstr);
            $stmt->bindParam("id", $nextid);
            $stmt->bindParam("name", $alias);
            $stmt->bindParam("parent", $parent);
            $stmt->bindParam("timemodified", $timemodified);
            $stmt->bindParam("depth", $depth);
            $stmt->bindParam("path", $path);
            $stmt->execute();

            $this->connection = NULL; //Cierra la conexion
            
            $data['resultado'] = true;
        } catch(PDOException $e){
            $data['resultado'] = $e->getMessage() ;
        }

        return $data;
    }


    public function createUser($request){
        /* Los usuarios se crean con una contraseña genérica que debe ser cambiada en la primera autenticación, esta configuración
        se almacena en la tabla mdl_user_preferences

        Se debe asignar un pwd generico para el primer acceso
         */


        $lang = 'es';
        $description = 'User created by API++';

        $data = $request->getParsedBody();

        $username = filter_var($data['username'], FILTER_SANITIZE_STRING);
        $password = '7YYHHggg&&668ybhlkhlh90797';
        $firstname = filter_var($data['firstname'], FILTER_SANITIZE_STRING);
        $lastname = filter_var($data['lastname'], FILTER_SANITIZE_STRING);
        $email = filter_var($data['email'], FILTER_SANITIZE_STRING);
        $city = filter_var($data['city'], FILTER_SANITIZE_STRING);
        $country = filter_var($data['country'], FILTER_SANITIZE_STRING);
        $timezone = filter_var($data['timezone'], FILTER_SANITIZE_STRING);
        $role = filter_var($data['role'], FILTER_SANITIZE_STRING);

        try{
            $this->con_obj = new db();
            $this->connection = $this->con_obj->conectar();

            //Insert MDL_U_COMPANY
            $sqlstr = "INSERT INTO mdl_user (
                `username`,
                `password`,
                `firstname`,
                `lastname`,
                `email`,
                `city`,
                `country`,
                `lang`,
                `timezone`,
                `description`,
                `timecreated`,
                `timemodified`
                ) 
            VALUES (
                :username,
                :password,
                :firstname,
                :lastname,
                :email,
                :city,
                :country,
                :lang,
                :timezone,
                :description,
                :timecreated,
                :timemodified
                )";
            
            $time = time();

            $stmt = $this->connection->prepare($sqlstr);
            $stmt->bindParam("username", $username);
            $stmt->bindParam("password", $password);
            $stmt->bindParam("firstname", $firstname);
            $stmt->bindParam("lastname", $lastname);
            $stmt->bindParam("email", $email);
            $stmt->bindParam("city", $city);
            $stmt->bindParam("country", $country);
            $stmt->bindParam("lang", $lang);
            $stmt->bindParam("timezone", $timezone);
            $stmt->bindParam("description", $description);
            $stmt->bindParam("timecreated", $time);
            $stmt->bindParam("timemodified", $time);
            $stmt->execute();
            
            $last_id = $this->connection->lastInsertId();
            //$last_id = 17333;

            //Get role information
            $id_role = $this->getIdRole($role);

            //MDL_USER_PREFERENCES
            //$preferences = $this->createUserPreferences($last_id);

            //MDL_U_USER_ADDITIONAL_INFO
            $additional = $this->createUserAdditionalInfo($request, $last_id);
            
            $this->connection = NULL; //Cierra la conexion
            
            $data['resultado'] = true;
            $data['lastId'] = $last_id;
            //$data['preferences'] = $preferences;
            $data['additional'] = $additional;

        } catch(PDOException $e){
            $data['resultado'] = $e->getMessage();
            $data['lastId'] = "error";
        }
        return $data;

    }//fin public function insertAgenda

    /* public function createUserPreferences($userid){
                
        try{
            $this->con_obj = new db();
            $this->connection = $this->con_obj->conectar();
            
            //Insert MDL_U_COMPANY
            $sqlstr = "INSERT INTO mdl_user_preferences (
                `userid`,
                `name`,
                `value`
                ) 
            VALUES (
                :userid, 
                :name, 
                :value
                )";
            
            $stmt = $this->connection->prepare($sqlstr);

            $name = "auth_forcepasswordchange";
            $value = 1;
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $name);
            $stmt->bindParam("value", $value);
            $stmt->execute();
            
            $stmt->bindParam("userid", $userid);
            $dato1 = "core_message_migrate_data";
            $dato2 = 1;
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
                
            $dato1 = "course_view_state";
            $dato2 = "grid";
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
            
            $dato1 = "email_bounce_count";
            $dato2 = 1;
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
                        
            $dato1 = "email_send_count";
            $dato2 = 1;
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
                        
            $dato1 = "filemanager_recentviewmode";
            $dato2 = 2;
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
                        
            $dato1 = "filepicker_recentlicense";
            $dato2 = "allrightsreserved";
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
                        
            $dato1 = "filepicker_recentrepository";
            $dato2 = 3;
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
                        
            $dato1 = "login_failed_count_since_success";
            $dato2 = 9;
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute();
                        
            $dato1 = "menubar_state";
            $dato2 = "unfold";
            $stmt->bindParam("userid", $userid);
            $stmt->bindParam("name", $dato1);
            $stmt->bindParam("value", $dato2);
            $stmt->execute(); 

            $this->connection = NULL; //Cierra la conexion
            
            $data['resultado'] = true;
        } catch(PDOException $e){
            $data['resultado'] = $e->getMessage() ;
        }

        return $data;
    } */

    public function createUserAdditionalInfo($request, $user_id){
        $data = $request->getParsedBody();

        $mdl_u_company_id = filter_var($data['company_id'], FILTER_SANITIZE_STRING);
        $company_area = filter_var($data['company_area'], FILTER_SANITIZE_STRING);
        $company_position = filter_var($data['company_position'], FILTER_SANITIZE_STRING);
        $company_position_id = filter_var($data['company_position_id'], FILTER_SANITIZE_STRING);
        $company_area_id = filter_var($data['company_area_id'], FILTER_SANITIZE_STRING);
                
        try{
            $this->con_obj = new db();
            $this->connection = $this->con_obj->conectar();
            
            //Insert MDL_U_COMPANY
            $sqlstr = "INSERT INTO mdl_u_user_additional_info (
                `mdl_u_company_id`,
                `mdl_user_id`,
                `company_area`,
                `company_position`,
                `company_position_id`,
                `company_area_id`
                ) 
            VALUES (
                :mdl_u_company_id,
                :mdl_user_id,
                :company_area,
                :company_position,
                :company_position_id,
                :company_area_id
                )";
            
            $stmt = $this->connection->prepare($sqlstr);

            $stmt->bindParam("mdl_u_company_id", $mdl_u_company_id);
            $stmt->bindParam("mdl_user_id", $user_id);
            $stmt->bindParam("company_area", $company_area);
            $stmt->bindParam("company_position", $company_position);
            $stmt->bindParam("company_position_id", $company_position_id);  #TODO, como obtiene los id anteriores, agregar dos end points
            $stmt->bindParam("company_area_id", $company_area_id);          #TODO, como obtiene los id anteriores, agregar dos end points
            $stmt->execute();
            
            $this->connection = NULL; //Cierra la conexion
            
            $data['resultado'] = true;
        } catch(PDOException $e){
            $data['resultado'] = $e->getMessage() ;
        }
        return $data;
    }

    public function getIdRole($shortname){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT * FROM mdl_role WHERE shortname = '$shortname'";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetch(PDO::FETCH_NUM);
            $this->connection = NULL; //Cierra la conexion
            return (int)$resp[0];

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }    


}//fin class studentModel

