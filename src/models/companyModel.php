<?php


/**
 * 
 */
class companyModel extends db{
    private $con_obj;
    private $connection;

    public function __construct(){
        
    }

    public function companyById($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT * 
        FROM mdl_u_company 
        WHERE id = '$id_company'";

        try{
            $execute = $this->connection->query($sqlstr);
            $students = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $students;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getQuantityIniciatedCourses($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT COUNT(mul.courseid) AS started
        FROM mdl_user_lastaccess mul
        LEFT JOIN mdl_user mu ON mul.userid = mu.id
        LEFT JOIN mdl_u_user_additional_info muuai ON mul.userid = muuai.mdl_user_id
        LEFT JOIN mdl_u_company muc ON muuai.mdl_u_company_id = muc.id
        LEFT JOIN mdl_course mc ON mc.id = mul.courseid
        LEFT JOIN mdl_u_private_course mupc ON mc.id = mupc.course_id AND muc.id = mupc.company_id
        WHERE muc.id = $id_company
        AND mu.deleted IS FALSE
        AND mc.visible IS TRUE
        AND mu.deleted IS FALSE
        AND mu.suspended IS FALSE
        AND mupc.course_id is null";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $resp;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getListIniciatedCourses($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT mc.id, mc.fullname, mc.shortname, mc.visible
        FROM mdl_user_lastaccess mul
        LEFT JOIN mdl_user mu ON mul.userid = mu.id
        LEFT JOIN mdl_u_user_additional_info muuai ON mul.userid = muuai.mdl_user_id
        LEFT JOIN mdl_u_company muc ON muuai.mdl_u_company_id = muc.id
        LEFT JOIN mdl_course mc ON mc.id = mul.courseid
        LEFT JOIN mdl_u_private_course mupc ON mc.id = mupc.course_id AND muc.id = mupc.company_id
        WHERE muc.id = $id_company
        AND mu.deleted IS FALSE
        AND mc.visible IS TRUE
        AND mu.deleted IS FALSE
        AND mu.suspended IS FALSE
        AND mupc.course_id is null";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $resp;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getQuantityFinishedCourses($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT COUNT(DISTINCT ci.id) as finished
        FROM mdl_certificate_issues ci
            LEFT JOIN mdl_user mu ON ci.userid = mu.id
            LEFT JOIN mdl_u_user_additional_info muuai ON mu.id = muuai.mdl_user_id
            LEFT JOIN mdl_u_company muc ON muc.id = muuai.mdl_u_company_id
            LEFT JOIN mdl_certificate ce ON (ci.certificateid = ce.id)
            LEFT JOIN mdl_certificate mc ON ci.certificateid = mc.id
            LEFT JOIN mdl_course mmc ON mc.course = mmc.id
            LEFT JOIN mdl_u_private_course mupc ON mmc.id = mupc.course_id AND muc.id = mupc.company_id
        WHERE muc.id = $id_company
            AND mu.deleted IS FALSE
            AND mmc.visible IS TRUE
            AND mu.deleted IS FALSE
            AND mu.suspended IS FALSE";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $resp;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getListFinishedCourses($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT mmc.id, mmc.fullname, mmc.shortname, mmc.visible
        FROM mdl_certificate_issues ci
            LEFT JOIN mdl_user mu ON ci.userid = mu.id
            LEFT JOIN mdl_u_user_additional_info muuai ON mu.id = muuai.mdl_user_id
            LEFT JOIN mdl_u_company muc ON muc.id = muuai.mdl_u_company_id
            LEFT JOIN mdl_certificate ce ON (ci.certificateid = ce.id)
            LEFT JOIN mdl_certificate mc ON ci.certificateid = mc.id
            LEFT JOIN mdl_course mmc ON mc.course = mmc.id
            LEFT JOIN mdl_u_private_course mupc ON mmc.id = mupc.course_id AND muc.id = mupc.company_id
        WHERE muc.id = $id_company
            AND mu.deleted IS FALSE
            AND mmc.visible IS TRUE
            AND mu.deleted IS FALSE
            AND mu.suspended IS FALSE
            AND mupc.course_id IS NOT NULL";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $resp;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getAllStudentsTime($id_company, $start_date, $finish_date){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT round(sum(mins_time),2) as time_mins
        FROM mdl_u_user_time ut
        LEFT JOIN mdl_course c ON c.id = ut.course_id
        LEFT JOIN mdl_u_user_additional_info muuac on ut.mdl_user_id = muuac.mdl_user_id
        LEFT JOIN mdl_u_compulsory_course AS mucc on (mucc.user_id = ut.mdl_user_id AND mucc.course_id = ut.course_id)
        LEFT JOIN mdl_u_private_course mupc ON ut.course_id = mupc.course_id
        LEFT JOIN mdl_course_categories cc ON c.category = cc.id
        LEFT JOIN mdl_u_user_subcategory AS us ON (cc.id = us.id_sc_category AND us.id_sc_user = ut.mdl_user_id)
        WHERE muuac.mdl_u_company_id = $id_company
        AND (ut.start_date IS NULL OR CAST(ut.start_date AS DATE) BETWEEN '$start_date' AND '$finish_date')
        AND (ut.end_date IS NULL OR CAST(ut.end_date AS DATE) BETWEEN '$start_date' AND '$finish_date')
        AND cc.visible IS TRUE;";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            return $resp;
        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }

    }


    public function getAccomplishmentPercentage($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "select count(0) * c.min_time_on_platform as expected_time 
        from  mdl_user u
        inner join mdl_u_user_additional_info muuai on u.id = muuai.mdl_u_company_id
        inner join mdl_u_company c on c.id = muuai.mdl_u_company_id
        where u.deleted = 0
        and c.id =$id_company";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            return $resp;
        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }

    }

}//fin class companyModel

