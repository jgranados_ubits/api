<?php


/**
 * 
 */
class studentsModel extends db{
    private $con_obj;
    private $connection;

    public function __construct(){
        
    }

    public function studentIdCompany($id_user){

        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT company_id 
        FROM view_info_user 
        WHERE user_id = '$id_user'";

        try{
            $execute = $this->connection->query($sqlstr);
            $students = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $students;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function studentByUserName($username){

        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT * 
        FROM view_info_user 
        WHERE username = '$username'";

        try{
            $execute = $this->connection->query($sqlstr);
            $students = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $students;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function studentById($id_user){

        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT * 
        FROM view_info_user 
        WHERE user_id = '$id_user'";

        try{
            $execute = $this->connection->query($sqlstr);
            $students = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $students;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function studentByEmail($email){

        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT * 
        FROM view_info_user 
        WHERE email = '$email'";

        try{
            $execute = $this->connection->query($sqlstr);
            $students = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $students;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function studentMonthlyGoal($id_user){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT round(min_time_on_platform / 12,0) as monthly_min_time_on_platform
        FROM mdl_u_user_additional_info muuai
        INNER JOIN mdl_u_company muc ON muuai.mdl_u_company_id = muc.id
        WHERE mdl_user_id = '$id_user'";

        try{
            $execute = $this->connection->query($sqlstr);
            $students = $execute->fetch(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $students;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function studentAnnualGoal($id_user){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT min_time_on_platform
        FROM mdl_u_company muc
        INNER JOIN mdl_u_user_additional_info muuai on muc.id = muuai.mdl_u_company_id
        WHERE muuai.mdl_user_id = $id_user";

        try{
            $execute = $this->connection->query($sqlstr);
            $students = $execute->fetch(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $students;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

public function getCoursesTime($id_user, $compulsory = '', $private = '', $start_date, $finish_date){
        $sql_complement = '';
        $sqlstr = '';
        $this->con_obj = new db();
        

        $id_company = $this->studentIdCompany($id_user);

        $this->connection = $this->con_obj->conectar();
        $call_sp = "CALL generate_company_times_and_finished_courses_table(".$id_company[0]->company_id.", '0', '1000000', '$start_date', '$finish_date');
        
        ";

        try{
            $execute = $this->connection->query($call_sp);
            $var = $execute->fetch(PDO::FETCH_OBJ);
         } catch(PDOException $e){
            //echo "Error: ".$e->getMessage();
        }
 
        $call_sp = "SELECT total_time as time 
        FROM mdl_ut_company_times_and_finished_courses
        WHERE user_id = $id_user";
        
        try{
            $execute = $this->connection->query($call_sp);
            $monthly_goal = $execute->fetch(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion

            return $monthly_goal->time;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }


    }

    public function getListCoursesTime($id_user, $compulsory = '', $private = '', $start_date, $finish_date){

        if(empty($start_date) && empty($finish_date)){
            echo "Please write the initial and/or ending date";
            return;
        }

        $sql_complement = '';
        $sqlstr = '';
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();
        
        $sqlstr = "SELECT ut.course_id, c.fullname, c.shortname, mucc.id as compulsory, mupc.course_id as private, round(sum(mins_time),2) as time
        FROM mdl_u_user_time ut
        LEFT JOIN mdl_course c ON c.id = ut.course_id
        LEFT JOIN mdl_u_compulsory_course AS mucc on (mucc.user_id = ut.mdl_user_id AND mucc.course_id = ut.course_id)
        LEFT JOIN mdl_u_private_course mupc ON ut.course_id = mupc.course_id
        LEFT JOIN mdl_course_categories cc ON c.category = cc.id
        LEFT JOIN mdl_u_user_subcategory AS us ON (cc.id = us.id_sc_category AND us.id_sc_user = ut.mdl_user_id)
        WHERE ut.mdl_user_id = $id_user
        AND (ut.start_date IS NULL OR CAST(ut.start_date AS DATE) BETWEEN '$start_date' AND '$finish_date')
        AND (ut.end_date IS NULL OR CAST(ut.end_date AS DATE) BETWEEN '$start_date' AND '$finish_date')
        AND cc.visible IS TRUE";

        if (!empty($compulsory)){
            $sql_complement .= " AND mucc.id IS NOT NULL";
        }

        if (!empty($private)){
            $sql_complement .= " AND mupc.course_id IS NOT NULL";
        }
        
        $sqlstr .= $sql_complement. " group by ut.course_id, c.fullname, c.shortname, mucc.id, mupc.course_id";

        try{
            $execute = $this->connection->query($sqlstr);
            $monthly_goal = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $monthly_goal;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage()."<br>".$sqlstr;
        }
    }

    public function getCompulsoryCoursesTime($id_user, $start_date, $finish_date){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT sum(muut.mins_time) as time_mins
        FROM mdl_u_user_time muut
        LEFT JOIN mdl_u_compulsory_course mucc on muut.course_id = mucc.course_id
        WHERE muut.mdl_user_id = $id_user
        AND mucc.user_id = muut.mdl_user_id
        AND (muut.start_date IS NULL OR CAST(muut.start_date AS DATE) BETWEEN '$start_date' AND '$finish_date')
        AND (muut.end_date IS NULL OR CAST(muut.end_date AS DATE) BETWEEN '$start_date' AND '$finish_date')";

        try{
            $execute = $this->connection->query($sqlstr);
            $monthly_goal = $execute->fetch(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $monthly_goal;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getListCompulsoryCoursesTime($id_user, $start_date, $finish_date){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT c.id, c.fullname, sum(muut.mins_time) as time_mins
        FROM mdl_u_user_time muut
        LEFT JOIN mdl_u_compulsory_course mucc on muut.course_id = mucc.course_id
        LEFT JOIN mdl_course c on mucc.course_id = c.id
        WHERE muut.mdl_user_id = $id_user
        AND mucc.user_id = muut.mdl_user_id
        AND (muut.start_date IS NULL OR CAST(muut.start_date AS DATE) BETWEEN '$start_date' AND '$finish_date')
        AND (muut.end_date IS NULL OR CAST(muut.end_date AS DATE) BETWEEN '$start_date' AND '$finish_date')
        GROUP BY c.id";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $resp;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function coursesByStudent($id_user){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT DISTINCT e.courseid, c.fullname, c.shortname 
        FROM mdl_user_enrolments mue
        INNER JOIN mdl_enrol e on mue.enrolid = e.id
        INNER JOIN mdl_course c on e.courseid = c.id
        WHERE mue.userid = $id_user
        AND c.visible = true";

        try{
            $execute = $this->connection->query($sqlstr);
            $resp = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $resp;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getListApprovedCourses($id_user, $start_date, $finish_date){

        $sqlstr = '';
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT distinct mmc.id, mmc.fullname, mmc.shortname, IF(mucc.id is null, 0, 1) as compulsory, 
            CAST(FROM_UNIXTIME(ci.timecreated) as date) AS finished_date
            FROM mdl_certificate_issues ci
            LEFT JOIN mdl_user mu ON ci.userid = mu.id
            LEFT JOIN mdl_u_user_additional_info muuai ON mu.id = muuai.mdl_user_id
            LEFT JOIN mdl_u_company muc ON muc.id = muuai.mdl_u_company_id
            LEFT JOIN mdl_certificate ce ON (ci.certificateid = ce.id)
            LEFT JOIN mdl_certificate mc ON ci.certificateid = mc.id
            LEFT JOIN mdl_course mmc ON mc.course = mmc.id
            LEFT JOIN mdl_u_compulsory_course mucc on (mmc.id = mucc.course_id and mucc.user_id = mu.id)
            WHERE mu.id = $id_user
            AND (ci.timecreated IS NULL OR CAST(FROM_UNIXTIME(ci.timecreated) AS DATE) BETWEEN '$start_date' AND '$finish_date')
            AND mmc.visible IS TRUE
            AND mu.deleted IS FALSE
            AND mu.suspended IS FALSE";

        try{
            $execute = $this->connection->query($sqlstr);
            $approbed_courses = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $approbed_courses;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage()."<br>".$sqlstr;
        }
    }

}//fin class studentModel

