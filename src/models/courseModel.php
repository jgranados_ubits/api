<?php

/**
 * 
 */
class courseModel extends db{
    private $con_obj;
    private $connection;

    public function __construct(){
        
    }

    public function getCourse($id_course){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT c.id, c.fullname, c.shortname, c.category, cc.name as category_name, c.summary, c.lang, c.timecreated, c.timemodified
        FROM mdl_course c
        INNER JOIN mdl_course_categories cc on c.category = cc.id
        WHERE c.id = $id_course";

        try{
            $execute = $this->connection->query($sqlstr);
            $info_course = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $info_course;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function listUbitsCourses(){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT c.id, c.fullname, c.shortname, c.category, cc.name as category_name, c.summary, c.lang, c.timecreated, c.timemodified
        FROM mdl_course c
        INNER JOIN mdl_course_categories cc on c.category = cc.id
        LEFT JOIN mdl_u_private_course mupc ON c.id = mupc.course_id
        WHERE c.visible IS TRUE
        AND mupc.course_id IS NULL";

        try{
            $execute = $this->connection->query($sqlstr);
            $list_ubits_courses = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $list_ubits_courses;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function quantityUbitsCourses(){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT DISTINCT COUNT(0) as cant
        FROM mdl_course c
        LEFT JOIN mdl_u_private_course mupc ON c.id = mupc.course_id
        WHERE c.visible IS TRUE
        AND mupc.course_id IS NULL";

        try{
            $execute = $this->connection->query($sqlstr);
            $qty_ubits_courses = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $qty_ubits_courses;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getListOwnCourses($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT c.id,  c.fullname, c.shortname, mupc.active
        FROM mdl_u_private_course mupc
        INNER JOIN mdl_course c on mupc.course_id = c.id
        WHERE mupc.company_id = $id_company
        AND c.visible = 1";

        try{
            $execute = $this->connection->query($sqlstr);
            $list_own_courses = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $list_own_courses;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getQuantityOwnCourses($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT COUNT(distinct mmc.id) as cant_own_courses
        FROM mdl_certificate_issues ci
         LEFT JOIN mdl_user mu ON ci.userid = mu.id
         LEFT JOIN mdl_u_user_additional_info muuai ON mu.id = muuai.mdl_user_id
         LEFT JOIN mdl_certificate mc ON ci.certificateid = mc.id
         LEFT JOIN mdl_course mmc ON mc.course = mmc.id
         LEFT JOIN mdl_u_compulsory_course cc ON (cc.course_id = mmc.id AND (cc.user_id = muuai.mdl_user_id))
        WHERE muuai.mdl_u_company_id = $id_company
          AND mu.deleted IS FALSE
          AND mmc.visible IS TRUE
          AND mu.deleted IS FALSE
          AND mu.suspended IS FALSE
          AND cc.id IS NOT NULL";

        try{
            $execute = $this->connection->query($sqlstr);
            $quantity_own_courses = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $quantity_own_courses;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

    public function getQuantityCompulsoryCoursesStudents($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT mu.id as user_id, mu.username, mu.firstname, mu.lastname, mu.email, mu.city, mu.country, 
        count(distinct(cc.course_id)) as cant_compulsory_courses
        FROM mdl_certificate_issues ci
        INNER JOIN mdl_user mu ON ci.userid = mu.id
        INNER JOIN mdl_u_user_additional_info muuai ON mu.id = muuai.mdl_user_id
        INNER JOIN mdl_certificate mc ON ci.certificateid = mc.id
        INNER JOIN mdl_course mmc ON mc.course = mmc.id
        INNER JOIN mdl_u_compulsory_course cc ON (cc.course_id = mmc.id AND (cc.user_id = muuai.mdl_user_id))
        WHERE muuai.mdl_u_company_id = $id_company
        AND mu.deleted IS FALSE
        AND mmc.visible IS TRUE
        AND mu.deleted IS FALSE
        AND mu.suspended IS FALSE
        AND cc.id IS NOT NULL
        GROUP BY mu.id, mu.username, mu.firstname, mu.lastname, mu.email, mu.city, mu.country";

        try{
            $execute = $this->connection->query($sqlstr);
            $qty_compulsory_courses = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $qty_compulsory_courses;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    } 

    public function getListCompulsoryCoursesStudents($id_company){
        $this->con_obj = new db();
        $this->connection = $this->con_obj->conectar();

        $sqlstr = "SELECT u.id as user_id, u.username, u.firstname, u.lastname, u.email, u.city, u.country, c.id as course_id, c.fullname, c.shortname, c.summary
        FROM mdl_u_compulsory_course cc
        INNER JOIN mdl_user u on cc.user_id = u.id
        INNER JOIN mdl_u_user_additional_info muuai on muuai.mdl_user_id = u.id
        INNER JOIN mdl_course c on cc.course_id = c.id
        WHERE u.deleted is false
        and c.visible is true
        and muuai.mdl_u_company_id = $id_company
        order by u.lastname, u.firstname";

        try{
            $execute = $this->connection->query($sqlstr);
            $qty_compulsory_courses = $execute->fetchAll(PDO::FETCH_OBJ);
            $this->connection = NULL; //Cierra la conexion
            
            return $qty_compulsory_courses;

        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    } 

}//fin class courseModel